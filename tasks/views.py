from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from .models import Task
from .forms import TaskForm
from tracker.urls import redirect_to_home


@login_required()
def create_task_view(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect_to_home(request)
    else:
        form = TaskForm
    context = {
        "form": form,
    }
    return render(request, "tasks/create_task.html", context)


@login_required()
def task_list_view(request):
    task_list = Task.objects.filter(assignee=request.user)
    context = {
        "task_list": task_list,
    }
    return render(request, "tasks/task_list.html", context)
