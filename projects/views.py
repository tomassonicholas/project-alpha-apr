from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from tracker.urls import redirect_to_home
from .forms import (
    ProjectForm,
)
from .models import (
    Project,
)


# Create your views here.
@login_required()
def project_list_view(request):
    project_list = Project.objects.filter(owner=request.user).all()
    context = {
        "project_list": project_list,
    }
    return render(request, "projects/project_list.html", context)


@login_required()
def project_detail_view(request, id):
    project = get_object_or_404(Project, id=id)
    context = {
        "project": project,
    }
    return render(request, "projects/project_detail.html", context)


@login_required()
def create_project_view(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect_to_home(request)
    else:
        form = ProjectForm
    context = {
        "form": form,
    }
    return render(request, "projects/create_project.html", context)
